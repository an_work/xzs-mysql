package com.mindskip.xzs.controller.wx.student;

import com.mindskip.xzs.base.BaseApiController;
import com.mindskip.xzs.base.RestResponse;
import com.mindskip.xzs.domain.WxVehicleInspection;
import com.mindskip.xzs.domain.WxVideoConfig;
import com.mindskip.xzs.service.WxVehicleInspectionService;
import com.mindskip.xzs.service.WxVideoConfigService;
import com.mindskip.xzs.viewmodel.wx.student.user.WxVehicleInspectionUpdateVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController("WXStudentWxVehicleInspectionController")
@RequestMapping(value = {"/api/student/vehicleInspection","/api/wx/vehicleInspection"})
public class WxVehicleInspectionController extends BaseApiController {

    private final WxVehicleInspectionService wxVehicleInspectionService;
    private final WxVideoConfigService wxVideoConfigService;

    @Autowired
    public WxVehicleInspectionController(WxVehicleInspectionService wxVehicleInspectionService,WxVideoConfigService wxVideoConfigService) {
        this.wxVehicleInspectionService = wxVehicleInspectionService;
        this.wxVideoConfigService = wxVideoConfigService;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public RestResponse edit(@RequestBody @Valid WxVehicleInspectionUpdateVM model) {
        //直接新增，无修改（需求说这个功能是由现场执行做的，不需要司机操作）
        wxVehicleInspectionService.insertWxVehicleInspection(model, 0L);  //取消token就不赋值用户id
        return RestResponse.ok();
    }

    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public RestResponse select() {
        WxVehicleInspection wxVehicleInspection = wxVehicleInspectionService.queryObjByUserId(getCurrentUser().getId());
        return RestResponse.ok(wxVehicleInspection);
    }

    //todo 数据库配置
    @RequestMapping(value = "/getVideoUrl", method = RequestMethod.POST)
    public RestResponse getVideoUrl() {
        WxVideoConfig wxVideoConfig = wxVideoConfigService.getVideoUrlByName("车检视频");
        return RestResponse.ok(wxVideoConfig.getVideoUrl());
    }
}
