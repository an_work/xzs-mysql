package com.mindskip.xzs.domain.enums;

public enum WxVehicleInspectionStatusEnum {

    INVALID(0,"无效"),
    VALID(1,"有效");

    int code;
    String name;

    WxVehicleInspectionStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
