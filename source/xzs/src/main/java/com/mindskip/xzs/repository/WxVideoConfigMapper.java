package com.mindskip.xzs.repository;

import com.mindskip.xzs.domain.WxVideoConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WxVideoConfigMapper extends BaseMapper<WxVideoConfig> {
    int deleteByPrimaryKey(Long id);

    int insert(WxVideoConfig record);

    int insertSelective(WxVideoConfig record);

    WxVideoConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WxVideoConfig record);

    int updateByPrimaryKey(WxVideoConfig record);

    WxVideoConfig getVideoUrlByName(String name);
}