package com.mindskip.xzs.configuration.spring.wx;

import com.mindskip.xzs.base.SystemCode;
import com.mindskip.xzs.configuration.spring.security.RestUtil;
import com.mindskip.xzs.context.WxContext;
import com.mindskip.xzs.domain.BaseUser;
import com.mindskip.xzs.domain.User;
import com.mindskip.xzs.service.BaseUserService;
import com.mindskip.xzs.service.UserService;
import com.mindskip.xzs.service.UserTokenService;
import com.mindskip.xzs.utility.TokenUtil;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TokenHandlerInterceptor implements HandlerInterceptor {

    private final UserTokenService userTokenService;
    private final UserService userService;
    private final WxContext wxContext;
    private final BaseUserService baseuserService;

    @Autowired
    public TokenHandlerInterceptor(UserTokenService userTokenService, UserService userService, WxContext wxContext,BaseUserService baseuserService) {
        this.userTokenService = userTokenService;
        this.userService = userService;
        this.wxContext = wxContext;
        this.baseuserService = baseuserService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        System.out.println("token:"+token);
        if (StringUtils.isEmpty(token) || StringUtils.isBlank(token)) {
            RestUtil.response(response, SystemCode.AccessTokenError);
            return false;
        }
        //解析token,获取用户id校验
        Claims claims = null;
        try{
            claims = TokenUtil.parseToken(token);
        }catch (Exception e){
            RestUtil.response(response, SystemCode.AccessTokenError);
            e.printStackTrace();
            return false;
        }
        String currentUser = claims.get("wxliteCurrentUser").toString();  //微信小程序token用户id
        if (StringUtils.isBlank(currentUser) || StringUtils.isEmpty(currentUser)){
            RestUtil.response(response, SystemCode.AccessTokenError);
            return false;
        }
//        User user = userService.selectById(Long.parseLong(currentUser));
        BaseUser baseUser = baseuserService.selectById(Long.parseLong(currentUser));  //微信司机用户表
        if (null == baseUser) {
            RestUtil.response(response, SystemCode.AccessTokenError);
            return false;
        }
        System.out.println("currentUser:"+currentUser);
        User user = new User();
        user.setId(baseUser.getId());
        wxContext.setContext(user,null);
        return true;
//        Date now = new Date();
//        User user = userService.getUserByUserName(userToken.getUserName());
//        if (now.before(userToken.getEndTime())) {
//            wxContext.setContext(user,userToken);
//            return true;
//        } else {   //refresh token
//            UserToken refreshToken = userTokenService.insertUserToken(user);
//            RestUtil.response(response, SystemCode.AccessTokenError.getCode(), SystemCode.AccessTokenError.getMessage(), refreshToken.getToken());
//            return false;
//        }
    }
}
