package com.mindskip.xzs.service.impl;

import com.mindskip.xzs.domain.WxVideoConfig;
import com.mindskip.xzs.repository.WxVideoConfigMapper;
import com.mindskip.xzs.service.WxVideoConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author zhangshunan
 * @Date 2021/8/16
 */
@Service
public class WxVideoConfigServiceImpl extends BaseServiceImpl<WxVideoConfig> implements WxVideoConfigService {
    @Autowired
    private final WxVideoConfigMapper wxVideoConfigMapper;

    @Autowired
    public WxVideoConfigServiceImpl(WxVideoConfigMapper wxVideoConfigMapper) {
        super(wxVideoConfigMapper);
        this.wxVideoConfigMapper = wxVideoConfigMapper;
    }

    @Override
    public WxVideoConfig selectById(Long id) {
        return null;
    }

    @Override
    public WxVideoConfig getVideoUrlByName(String name) {
        return wxVideoConfigMapper.getVideoUrlByName(name);
    }
}
