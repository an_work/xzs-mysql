package com.mindskip.xzs.service.impl;

import com.mindskip.xzs.domain.WxVehicleInspection;
import com.mindskip.xzs.domain.enums.WxVehicleInspectionStatusEnum;
import com.mindskip.xzs.repositoryds2.WxVehicleInspectionMapper;
import com.mindskip.xzs.service.WxVehicleInspectionService;
import com.mindskip.xzs.utility.ModelMapperSingle;
import com.mindskip.xzs.viewmodel.wx.student.user.WxVehicleInspectionUpdateVM;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class WxVehicleInspectionServiceImpl extends BaseServiceImpl<WxVehicleInspection> implements WxVehicleInspectionService {

    protected final static ModelMapper modelMapper = ModelMapperSingle.Instance();
    private final WxVehicleInspectionMapper wxVehicleInspectionMapper;

    @Autowired
    public WxVehicleInspectionServiceImpl(WxVehicleInspectionMapper wxVehicleInspectionMapper) {
        super(wxVehicleInspectionMapper);
        this.wxVehicleInspectionMapper = wxVehicleInspectionMapper;
    }

    @Override
    public WxVehicleInspection selectById(Long id) {
        return null;
    }

    @Override
    public WxVehicleInspection insertWxVehicleInspection(WxVehicleInspectionUpdateVM model, Long userid) {
        Date now = new Date();
        WxVehicleInspection obj = new WxVehicleInspection();
        modelMapper.map(model,obj);
        obj.setUserId(userid);
        obj.setCreator(userid);
        obj.setCreateTime(now);
        obj.setUpdateTime(now);
        obj.setValid(WxVehicleInspectionStatusEnum.VALID.getCode());
        wxVehicleInspectionMapper.insertSelective(obj);
        return obj;
    }

    @Override
    public WxVehicleInspection updateWxVehicleInspection(WxVehicleInspectionUpdateVM model,WxVehicleInspection obj) {
        modelMapper.map(model,obj);
        Date now = new Date();
        obj.setUpdateTime(now);
        wxVehicleInspectionMapper.updateByPrimaryKeySelective(obj);
        return null;
    }

    @Override
    public WxVehicleInspection queryObjByUserId(Long id) {
        return wxVehicleInspectionMapper.queryObjByUserId(id);
    }
}
