package com.mindskip.xzs.service.impl;

import com.mindskip.xzs.domain.UserRole;
import com.mindskip.xzs.repository.BaseMapper;
import com.mindskip.xzs.repository.UserMapper;
import com.mindskip.xzs.repository.UserRoleMapper;
import com.mindskip.xzs.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRole> implements UserRoleService {

    private final UserRoleMapper userRoleMapper;

    @Autowired
    public UserRoleServiceImpl(UserRoleMapper userRoleMapper) {
        super(userRoleMapper);
        this.userRoleMapper = userRoleMapper;
    }

    @Override
    public UserRole getUserRoleByUserId(Long userId) {
        return userRoleMapper.getUserRoleByUserId(userId);
    }

    @Override
    public UserRole getUserRoleByUserIdAndRoleId(Long userId, Long roleId) {
        return userRoleMapper.getUserRoleByUserIdAndRoleId(userId,roleId);
    }

    @Override
    public UserRole getUserRoleByUserIdAndRoleName(Long userId, String roleName) {
        return userRoleMapper.getUserRoleByUserIdAndRoleName(userId,roleName);
    }

    @Override
    public UserRole selectById(Long id) {
        return null;
    }
}
