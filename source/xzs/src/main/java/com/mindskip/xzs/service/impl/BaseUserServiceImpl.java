package com.mindskip.xzs.service.impl;

import com.mindskip.xzs.domain.BaseUser;
import com.mindskip.xzs.repository.BaseUserMapper;
import com.mindskip.xzs.service.BaseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaseUserServiceImpl extends BaseServiceImpl<BaseUser> implements BaseUserService {

    private final BaseUserMapper baseUserMapper;

    @Autowired
    public BaseUserServiceImpl(BaseUserMapper baseUserMapper) {
        super(baseUserMapper);
        this.baseUserMapper = baseUserMapper;
    }

    @Override
    public BaseUser selectById(Long id) {
        return baseUserMapper.selectByPrimaryKey(id);
    }
}
