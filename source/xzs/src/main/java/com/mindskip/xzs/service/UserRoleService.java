package com.mindskip.xzs.service;

import com.mindskip.xzs.domain.UserRole;

public interface UserRoleService extends BaseService<UserRole> {
    /**
     * 用户id,获取角色
     * @param userId
     * @return
     */
    UserRole getUserRoleByUserId(Long userId);

    /**
     * 用户id，角色id,获取角色信息
     * @param userId
     * @return
     */
    UserRole getUserRoleByUserIdAndRoleId(Long userId,Long roleId);

    /**
     * 用户id，角色名,获取角色信息
     * @param userId
     * @return
     */
    UserRole getUserRoleByUserIdAndRoleName(Long userId,String roleName);
}
