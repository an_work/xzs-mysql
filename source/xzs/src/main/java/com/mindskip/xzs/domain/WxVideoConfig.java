package com.mindskip.xzs.domain;

import java.io.Serializable;

/**
 * wx_video_config
 * @author 
 */
public class WxVideoConfig implements Serializable {
    private Long id;

    /**
     * 视频地址
     */
    private String videoUrl;

    /**
     * 视频名称
     */
    private String name;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}