package com.mindskip.xzs.viewmodel.student.exam;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;


public class ExamPaperSubmitVehicleVM extends ExamPaperSubmitVM {

    /**
     * 姓名
     */
    @NotNull
    private String name;

    /**
     * 车牌号
     */
    @NotNull
    private String licensePlateNumber;

    /**
     * 承运商
     */
    private String commonCarrier;

    /**
     * 手机号码
     */
    @NotNull
    private Long phone;

    /**
     * 检查日期
     */
    private Date examinationTime;

    /**
     * 人车合影图片
     */
    private Long mancarImage;

    /**
     * 车头照片
     */
    private Long headstockImage;

    /**
     * 车尾照片
     */
    private Long tailstockImage;

    /**
     * 车板照片
     */
    private Long sweepImage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public String getCommonCarrier() {
        return commonCarrier;
    }

    public void setCommonCarrier(String commonCarrier) {
        this.commonCarrier = commonCarrier;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Date getExaminationTime() {
        return examinationTime;
    }

    public void setExaminationTime(Date examinationTime) {
        this.examinationTime = examinationTime;
    }

    public Long getMancarImage() {
        return mancarImage;
    }

    public void setMancarImage(Long mancarImage) {
        this.mancarImage = mancarImage;
    }

    public Long getHeadstockImage() {
        return headstockImage;
    }

    public void setHeadstockImage(Long headstockImage) {
        this.headstockImage = headstockImage;
    }

    public Long getTailstockImage() {
        return tailstockImage;
    }

    public void setTailstockImage(Long tailstockImage) {
        this.tailstockImage = tailstockImage;
    }

    public Long getSweepImage() {
        return sweepImage;
    }

    public void setSweepImage(Long sweepImage) {
        this.sweepImage = sweepImage;
    }
}
