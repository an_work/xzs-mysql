package com.mindskip.xzs.viewmodel.wx.student.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class WxVehicleInspectionUpdateVM {
    /**
     * 姓名
     */
    @NotBlank
    private String name;

    /**
     * 车牌号
     */
    @NotBlank
    private String licensePlateNumber;

    /**
     * 承运商
     */
    private String commonCarrier;

    /**
     * 手机号码
     */
    @NotNull
    private Long phone;

    /**
     * 检查日期
     */
    private String examinationTime;

    /**
     * 人车合影图片
     */
    private Long mancarImage;

    /**
     * 车头照片
     */
    private Long headstockImage;

    /**
     * 车尾照片
     */
    private Long tailstockImage;

    /**
     * 车板照片
     */
    private Long sweepImage;

    /**
     * 必须遵守当地法律法规(1：是，0：否)
     */
    private String col1;

    /**
     * 车辆和证件相互匹配
     */
    private String col2;

    /**
     * 车辆证明材料是否符合规定
     */
    private String col3;

    /**
     * 车胎压力是否充足
     */
    private String col4;

    /**
     * 无光头胎/磨损严重胎/无明显裂纹/切割/撕裂
     */
    private String col5;

    /**
     * 车灯状况良好
     */
    private String col6;

    /**
     * 喇叭工作正常
     */
    private String col7;

    /**
     * 货物防掉落设备设施齐全
     */
    private String col8;

    /**
     * 托盘/装卸货台/车厢底板和围栏/车门等结构良好、装载规范
     */
    private String col9;

    /**
     * 是否已告知司机HSSE有关规定
     */
    private String col10;

    /**
     * 车辆至少配备2个4公斤的灭火器材
     */
    private String col11;

    /**
     * 车辆配备有雪糕筒、反光三角架
     */
    private String col12;

    /**
     * 车辆挡风玻璃完好无损，视线良好
     */
    private String col13;

    /**
     * 司机和其他协助人员是否穿戴正确的、维护良好的个人防护用品
     */
    private String col14;

    /**
     * 进去壳牌现场时是否佩戴三点式安全带
     */
    private String col15;

    /**
     * 司机精神状态良好/无倦意/无饮酒迹象
     */
    private String col16;

    /**
     * 驾驶员日志填写规范
     */
    private String col17;

    /**
     * 司机是否携带和了解行程计划安排
     */
    private String col18;

    /**
     * 司机是否携带和了解道路风险图
     */
    private String col19;

    /**
     * 司机是否接受过救命规则的培训
     */
    private String col20;

    /**
     * 入厂前是否已完成司机培训及考卷
     */
    private String col21;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public String getCommonCarrier() {
        return commonCarrier;
    }

    public void setCommonCarrier(String commonCarrier) {
        this.commonCarrier = commonCarrier;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getExaminationTime() {
        return examinationTime;
    }

    public void setExaminationTime(String examinationTime) {
        this.examinationTime = examinationTime;
    }

    public Long getMancarImage() {
        return mancarImage;
    }

    public void setMancarImage(Long mancarImage) {
        this.mancarImage = mancarImage;
    }

    public Long getHeadstockImage() {
        return headstockImage;
    }

    public void setHeadstockImage(Long headstockImage) {
        this.headstockImage = headstockImage;
    }

    public Long getTailstockImage() {
        return tailstockImage;
    }

    public void setTailstockImage(Long tailstockImage) {
        this.tailstockImage = tailstockImage;
    }

    public Long getSweepImage() {
        return sweepImage;
    }

    public void setSweepImage(Long sweepImage) {
        this.sweepImage = sweepImage;
    }

    public String getCol1() {
        return col1;
    }

    public void setCol1(String col1) {
        this.col1 = col1;
    }

    public String getCol2() {
        return col2;
    }

    public void setCol2(String col2) {
        this.col2 = col2;
    }

    public String getCol3() {
        return col3;
    }

    public void setCol3(String col3) {
        this.col3 = col3;
    }

    public String getCol4() {
        return col4;
    }

    public void setCol4(String col4) {
        this.col4 = col4;
    }

    public String getCol5() {
        return col5;
    }

    public void setCol5(String col5) {
        this.col5 = col5;
    }

    public String getCol6() {
        return col6;
    }

    public void setCol6(String col6) {
        this.col6 = col6;
    }

    public String getCol7() {
        return col7;
    }

    public void setCol7(String col7) {
        this.col7 = col7;
    }

    public String getCol8() {
        return col8;
    }

    public void setCol8(String col8) {
        this.col8 = col8;
    }

    public String getCol9() {
        return col9;
    }

    public void setCol9(String col9) {
        this.col9 = col9;
    }

    public String getCol10() {
        return col10;
    }

    public void setCol10(String col10) {
        this.col10 = col10;
    }

    public String getCol11() {
        return col11;
    }

    public void setCol11(String col11) {
        this.col11 = col11;
    }

    public String getCol12() {
        return col12;
    }

    public void setCol12(String col12) {
        this.col12 = col12;
    }

    public String getCol13() {
        return col13;
    }

    public void setCol13(String col13) {
        this.col13 = col13;
    }

    public String getCol14() {
        return col14;
    }

    public void setCol14(String col14) {
        this.col14 = col14;
    }

    public String getCol15() {
        return col15;
    }

    public void setCol15(String col15) {
        this.col15 = col15;
    }

    public String getCol16() {
        return col16;
    }

    public void setCol16(String col16) {
        this.col16 = col16;
    }

    public String getCol17() {
        return col17;
    }

    public void setCol17(String col17) {
        this.col17 = col17;
    }

    public String getCol18() {
        return col18;
    }

    public void setCol18(String col18) {
        this.col18 = col18;
    }

    public String getCol19() {
        return col19;
    }

    public void setCol19(String col19) {
        this.col19 = col19;
    }

    public String getCol20() {
        return col20;
    }

    public void setCol20(String col20) {
        this.col20 = col20;
    }

    public String getCol21() {
        return col21;
    }

    public void setCol21(String col21) {
        this.col21 = col21;
    }
}
