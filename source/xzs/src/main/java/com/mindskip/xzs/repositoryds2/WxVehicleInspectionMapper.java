package com.mindskip.xzs.repositoryds2;

import com.mindskip.xzs.domain.WxVehicleInspection;
import com.mindskip.xzs.repository.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WxVehicleInspectionMapper extends BaseMapper<WxVehicleInspection> {
    int deleteByPrimaryKey(Long id);

    int insert(WxVehicleInspection record);

    int insertSelective(WxVehicleInspection record);

    WxVehicleInspection selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WxVehicleInspection record);

    int updateByPrimaryKey(WxVehicleInspection record);

    WxVehicleInspection queryObjByUserId(Long id);
}