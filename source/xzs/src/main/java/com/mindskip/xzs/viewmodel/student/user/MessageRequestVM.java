package com.mindskip.xzs.viewmodel.student.user;

import com.mindskip.xzs.base.BasePage;

public class MessageRequestVM extends BasePage {

    private Long receiveUserId;

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }
}
