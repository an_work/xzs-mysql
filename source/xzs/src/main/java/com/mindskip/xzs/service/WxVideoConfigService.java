package com.mindskip.xzs.service;

import com.mindskip.xzs.domain.WxVideoConfig;

/**
 * @Author zhangshunan
 * @Date 2021/8/16
 */
public interface WxVideoConfigService extends BaseService<WxVideoConfig> {
    WxVideoConfig getVideoUrlByName(String name);
}
