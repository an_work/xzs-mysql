package com.mindskip.xzs.configuration.spring.security;


import com.mindskip.xzs.context.WebContext;
import com.mindskip.xzs.domain.UserRole;
import com.mindskip.xzs.domain.enums.RoleEnum;
import com.mindskip.xzs.domain.enums.UserStatusEnum;
import com.mindskip.xzs.service.AuthenticationService;
import com.mindskip.xzs.service.UserRoleService;
import com.mindskip.xzs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * 登录用户名密码验证
 *
 * @author :  武汉思维跳跃科技有限公司
 * Description :  身份验证
 */

@Component
public class RestAuthenticationProvider implements AuthenticationProvider {

    private final AuthenticationService authenticationService;
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final WebContext webContext;

    @Autowired
    public RestAuthenticationProvider(AuthenticationService authenticationService, UserService userService, WebContext webContext,UserRoleService userRoleService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.webContext = webContext;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        com.mindskip.xzs.domain.User user = userService.getUserByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException("用户名或密码错误");
        }

        boolean result = authenticationService.authUser(user, username, password);
        if (!result) {
            throw new BadCredentialsException("用户名或密码错误");
        }

        UserStatusEnum userStatusEnum = UserStatusEnum.fromCode(user.getIfStatus());
        if (UserStatusEnum.Disable == userStatusEnum) {
            throw new LockedException("用户被禁用");
        }

        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        //todo 通过角色控制admin访问,目前写死角色名（答题管理角色）
//        UserRole userRole = userRoleService.getUserRoleByUserIdAndRoleId(user.getId(),1423125363773132801L);
        UserRole userRole = userRoleService.getUserRoleByUserIdAndRoleName(user.getId(),"答题管理角色");
        if(userRole != null){
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));  //管理员设置题目
        }else{
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
        }

        User authUser = new User(user.getLoginName(), user.getPassword(), grantedAuthorities);
        return new UsernamePasswordAuthenticationToken(authUser, authUser.getPassword(), authUser.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
