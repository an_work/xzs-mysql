package com.mindskip.xzs.repository;

import com.mindskip.xzs.domain.UserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
    int deleteByPrimaryKey(Long id);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);

    UserRole getUserRoleByUserId(Long userId);

    UserRole getUserRoleByUserIdAndRoleId(Long userId, Long roleId);

    UserRole getUserRoleByUserIdAndRoleName(Long userId, String roleName);
}