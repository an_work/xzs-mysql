package com.mindskip.xzs.domain;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    /**
     * 用户编号
     */
    private Long id;

    /**
     * 登录账号
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * 加密盐值
     */
    private String salt;

    /**
     * 用户名称
     */
    private String realName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 备注
     */
    private String remark;

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 用户状态（1：可用；0：锁定）
     */
    private Integer ifStatus;

    /**
     * 创建人编号
     */
    private Long creator;

    /**
     * 创建人时间
     */
    private Date createTime;

    /**
     * 修改人编号
     */
    private Long updator;

    /**
     * 修改人时间
     */
    private Date updateTime;

    /**
     * 是否有效
     */
    private Integer valid;

    /**
     * 默认机构代码
     */
    private String orgCode;

    /**
     * 租户编号
     */
    private String tenantCode;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getIfStatus() {
        return ifStatus;
    }

    public void setIfStatus(Integer ifStatus) {
        this.ifStatus = ifStatus;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdator() {
        return updator;
    }

    public void setUpdator(Long updator) {
        this.updator = updator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}