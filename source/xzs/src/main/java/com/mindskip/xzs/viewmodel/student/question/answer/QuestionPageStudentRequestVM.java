package com.mindskip.xzs.viewmodel.student.question.answer;

import com.mindskip.xzs.base.BasePage;

public class QuestionPageStudentRequestVM extends BasePage {
    private Long createUser;

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
}
