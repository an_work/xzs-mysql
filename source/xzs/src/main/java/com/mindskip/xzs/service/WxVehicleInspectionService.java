package com.mindskip.xzs.service;

import com.mindskip.xzs.domain.WxVehicleInspection;
import com.mindskip.xzs.viewmodel.wx.student.user.WxVehicleInspectionUpdateVM;

public interface WxVehicleInspectionService extends BaseService<WxVehicleInspection> {

    WxVehicleInspection insertWxVehicleInspection(WxVehicleInspectionUpdateVM model, Long userid);

    WxVehicleInspection updateWxVehicleInspection(WxVehicleInspectionUpdateVM model,WxVehicleInspection obj);

    WxVehicleInspection queryObjByUserId(Long id);
}
