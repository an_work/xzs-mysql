import { post } from '@/utils/request'

export default {
  pageList: query => post('/inspect/api/admin/question/page', query),
  edit: query => post('/inspect/api/admin/question/edit', query),
  select: id => post('/inspect/api/admin/question/select/' + id),
  deleteQuestion: id => post('/inspect/api/admin/question/delete/' + id)
}
