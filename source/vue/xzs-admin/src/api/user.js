import { post } from '@/utils/request'

export default {
  getUserPageList: query => post('/inspect/api/admin/user/page/list', query),
  getUserEventPageList: query => post('/inspect/api/admin/user/event/page/list', query),
  createUser: query => post('/inspect/api/admin/user/edit', query),
  selectUser: id => post('/inspect/api/admin/user/select/' + id),
  getCurrentUser: () => post('/inspect/api/admin/user/current'),
  updateUser: query => post('/inspect/api/admin/user/update', query),
  changeStatus: id => post('/inspect/api/admin/user/changeStatus/' + id),
  deleteUser: id => post('/inspect/api/admin/user/delete/' + id),
  selectByUserName: query => post('/inspect/api/admin/user/selectByUserName', query)
}
