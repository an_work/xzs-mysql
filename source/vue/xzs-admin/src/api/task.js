import { post } from '@/utils/request'

export default {
  pageList: query => post('/inspect/api/admin/task/page', query),
  edit: query => post('/inspect/api/admin/task/edit', query),
  select: id => post('/inspect/api/admin/task/select/' + id),
  deleteTask: id => post('/inspect/api/admin/task/delete/' + id)
}
