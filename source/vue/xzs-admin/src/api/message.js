import { post } from '@/utils/request'

export default {
  pageList: query => post('/inspect/api/admin/message/page', query),
  send: query => post('/inspect/api/admin/message/send', query)
}
