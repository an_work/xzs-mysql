import { post, postWithLoadTip } from '@/utils/request'

export default {
  login: query => postWithLoadTip(`/inspect/api/user/login`, query),
  logout: query => post(`/inspect/api/user/logout`, query)
}
