import { post } from '@/utils/request'

export default {
  index: () => post('/inspect/api/admin/dashboard/index')
}
