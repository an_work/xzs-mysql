import { post } from '@/utils/request'

export default {
  list: query => post('/inspect/api/admin/education/subject/list'),
  pageList: query => post('/inspect/api/admin/education/subject/page', query),
  edit: query => post('/inspect/api/admin/education/subject/edit', query),
  select: id => post('/inspect/api/admin/education/subject/select/' + id),
  deleteSubject: id => post('/inspect/api/admin/education/subject/delete/' + id)
}
