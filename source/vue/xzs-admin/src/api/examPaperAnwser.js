import { post } from '@/utils/request'

export default {
  page: query => post('/inspect/api/admin/examPaperAnswer/page', query)
}
