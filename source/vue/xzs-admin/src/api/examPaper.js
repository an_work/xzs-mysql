import { post } from '@/utils/request'

export default {
  pageList: query => post('/inspect/api/admin/exam/paper/page', query),
  taskExamPage: query => post('/inspect/api/admin/exam/paper/taskExamPage', query),
  edit: query => post('/inspect/api/admin/exam/paper/edit', query),
  select: id => post('/inspect/api/admin/exam/paper/select/' + id),
  deletePaper: id => post('/inspect/api/admin/exam/paper/delete/' + id)
}
